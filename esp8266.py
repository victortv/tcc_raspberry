# -*- coding: utf-8 -*-
import sys
import requests
import json
import sqlite3
import server

import os
import datetime

from flask import Flask, request
from flask_restful import Resource, Api
#from sqlalchemy import create_engine

from os import urandom
from Crypto.Hash import SHA256, HMAC
from Crypto.Cipher import AES

import base64

#Function to convert SQLite3 query to Dict
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

 
#Create Local Database Connection
conn = sqlite3.connect('local.db')
conn.text_factory = str
#conn.row_factory = dict_factory
cur = conn.cursor()


#Decrypt a payload
#Format = 'HEX' || 'BASE64'
def decrypt(payload = None, key = 'essa_e_minha_key_32_bits12345678', salt='essa_e_minha_salt_32_bits1234567', useintegrity = True, hashmode = 'HMAC-SHA256', format='HEX', doublehash = True, debug = True):

  #Converting payload to bytes
  if format == 'BASE64':
    print '\n\npayload in BASE64: ' + payload if format == debug else ''
    payload = base64.b64decode(payload)
    print '\npayload in HEX: ' + payload.encode('hex')
  elif format == 'HEX':
    print '\n\npayload in HEX: '
    print payload
    payload = payload.decode('hex')
    print '\npayload in BYTES: ' + payload
  
  iv = payload[:16]
  cipher = payload[16:-32]
  hash = payload[-32:]
  
  if useintegrity == True:  
    #Verify Integrity
    if hashmode == 'SHA256':
      calculatedHash = SHA256.new(iv+cipher).digest()
    elif hashmode == 'HMAC-SHA256':
      calculatedHash = HMAC.new(salt, msg=(iv+cipher), digestmod=SHA256).digest()  
    
    #Double Hash
    if doublehash:
      calculatedHash = HMAC.new(salt, msg=calculatedHash, digestmod=SHA256).digest()
      hash = HMAC.new(salt, msg=hash, digestmod=SHA256).digest()
    
    message = ''
    if calculatedHash == hash:
      message = AES.new(key, AES.MODE_CBC, iv).decrypt(cipher)
      print 'integrity confirmed'
    else:
      print 'calculatedHash: ' + calculatedHash + '\t\t' + calculatedHash.encode('hex')
      print 'wrong integrity'
  
  else:
    message = AES.new(key, AES.MODE_CBC, iv).decrypt(cipher)
  
  if debug:
    print '\n\n-------DECRYPT-------'
    print 'key: ' + key + '\t\t' + key.encode('hex')
    print 'salt: ' + salt + '\t\t' + salt.encode('hex')
    print '\npayload: ' + payload
    print 'IV: ' + iv
    print 'cipher: ' + cipher
    print 'hash: ' + hash
    print 'message: ' + message

  
  return message


#Encrypt with AES and HMAC
def encrypt(message = '{"command":{"name": "set","value": "true"}, "iv" : "'+urandom(16).encode('hex')+'", "counter": "8827362873629836"}', key = 'essa_e_minha_key_32_bits12345678', salt='essa_e_minha_salt_32_bits1234567', hashmode = 'HMAC-SHA256', format = 'HEX', debug = True):

  key = '315f7124ef11caa1849b54caf93079fd2ac42d591bb8a688aa8a8e6bb63d3bd8'.decode('hex')
  salt = 'fd07fbdd038174d0681fdb98357b4b460f7b55c8898a22c0ee519212eb07cccb'.decode('hex')
 # salt = '0000000000000000000000000000000000000000000000000000000000000000'.decode('hex')
  
  
  
  print len(message)
  message = message + (16-len(message)%16)*' '
  print len(message)
  #generate new 16 bytes IV
  iv = urandom(16)
  cipher = AES.new(key, AES.MODE_CBC, iv).encrypt(message)
  
  if hashmode == 'SHA256':
    hash = SHA256.new(iv+cipher).digest()
  elif hashmode == 'HMAC-SHA256':
    hash = HMAC.new(salt, msg=(iv+cipher), digestmod=SHA256).digest() 
  else:
    hash = ''
  
  payload = iv + cipher + hash
  
  
  if debug:
    print '----ENCRYPT-----'
    print '\tascii\t\t\t\t\tHEX'
    print 'key: ' + key + '\t\t' + key.encode('hex')
    print 'salt: ' + salt + '\t\t' + salt.encode('hex') + '\n'
    print 'message: ' + message + '\t\t' + message.encode('hex')
    print 'iv: ' + iv + '\t\t' + iv.encode('hex')
    print 'cipher: ' + cipher + '\t\t\t\t\t' + cipher.encode('hex')
    print 'hash: ' + hash + '\t\t' + hash.encode('hex')
    print '\n\n'
    print 'payload: ' + payload + '\t' + payload.encode('hex')
  
  if format == 'BASE64':
    payload = base64.b64encode(payload)
  elif format == 'HEX':
    payload = payload.encode('hex')
  
  
  f = open('log.txt','w')
  f.write(payload + '\n')
  f.write(key.encode('hex') + '\n')
  f.write(iv.encode('hex') + '\n')
  f.write(cipher.encode('hex') + '\n')
  f.write(hash.encode('hex') + '\n')
  f.close()
  return payload
  


#Create Restful API to receive GET requests from node
def run_api(host='0.0.0.0', port='5002'):
  app = Flask(__name__)
  api = Api(app)

  class NodeState(Resource):
    def get(self,state):
      
      incomingIp = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
      
      try:
        serverTime = str(server.getServerTime())
      except:
        serverTime = ''
        print 'can\'t get time form server'
      
      print "\n\nreceb? um GET\n"
      print 'O valor passado eh: ' + state + '   Seu IP eh: '  + incomingIp + '     timestamp do servidor eh:  ' + serverTime
      
      
      
      #Unconcatenate HMAC X CYPHER
      
      
      
      
      #Validate HMAC
      
      
      
      #If Valid, Decrypt cypher
      
      message = True
      
      #Check corresponding Ip to node ID at database
      #cur.execute('select id from nodes_info where url = ?',(url,))
      #id = cur.fetchall()[0][0]
      
      
      #Insert state into database
      
      #cur.execute('insert into nodes values ({},{},{})'.format(id,serverTime,message))
      
      return 'O valor passado eh: ' + state + '   Seu IP eh: '  + incomingIp + '     timestamp do servidor eh:  ' + serverTime

  api.add_resource(NodeState, '/<state>')    

  app.run(host=host, port=port)
   
    
# #Class Node - not used
# class node:
  # def __init__(self,url = 'http://localhost:3000/'):
    # self.url = url
    

nodes_info = []
#Load nodes table into a list
def load_nodes_info():
  cur.execute('CREATE TABLE IF NOT EXISTS nodes_info (id INTEGER PRIMARY KEY ASC, type TEXT, url TEXT, name TEXT)')
  conn.commit()
  cur.execute('SELECT * from nodes_info order by id')
  nodes_info = cur.fetchall()
  return nodes_info

  
#Register a node (type = bool | double | string | json)
def register_node(id = None, type = 'bool', url = 'http://localhost:3001/', param = '{}', name = ' ', aes=urandom(32).encode('hex'), salt=urandom(32).encode('hex'), ssid = 'SulzHall', password = '995547192'):
  #load list containing all Node IDs
  idList = zip(*load_nodes_info())[0]
  
  if id == None:
    cur.execute('SELECT max(id) from nodes_info')
    id = 1 + int(cur.fetchall()[0][0])
  
  
  
  if id in idList:
    print 'ja existe'
  else:
    print 'novo no: id={}, tipe={}, url={}, aes={}, salt={}'.format(id,type,url,param,name,aes,salt)
    cur.execute('insert into nodes_info values (?,?,?,?,?,?,?)',(id,type,url,param,name,str(aes),str(salt)))
    conn.commit()
    print 'novas lista de nos: '
    cur.execute('select * from nodes_info')
    print cur.fetchall()
    #cur.execute

  #function used to properly format a python hex string to a c++ declaration
  def encode_byte(var):
    out = '{'
    for i in var:
      out += '0x'+str(i.encode('hex'))+','
    out = out[:-1] #remove last ','
    out += '}'
    return out
  
  f = open('constants.h','w')
  f.write('\\\\constants.h\n'+ \
          'const char* c_id = \"' + str(id) + '\";\n' + \
          'const char* c_ssid = \"' + ssid + '\";\n' + \
          'const char* c_password = \"' + password + '\";\n' \
          'byte c_aesKey[32] = \"' + encode_byte(aes) + '\";\n' + \
          'byte c_salt[32] = \"' + encode_byte(salt) + '\";\n' \
          )
          

  
  
  f.close()
  
  
def read_node(id, cursor = cur):
  cursor.execute('SELECT * from nodes_info where id = {}'.format(id))
  print "noh lido"
  return cur.fetchall()[0]   

#Delete node by choosing its ID
def delete_node(id):
  cur.execute('delete from nodes_info where id = ?',id)
  conn.commit()

def update_node(id, type=None, url=None):
  if type <> None:
    cur.execute("""UPDATE nodes_info
                SET type = ?
                WHERE id = ?""",(type,id))
                
  if url <> None:
    cur.execute("""UPDATE nodes_info
                SET url = ?
                WHERE id = ?""",(type,id))    

                
#Read most recent nodes from Database
def load_nodes_state():
  nodes_data = []
  for row in cur.execute("select n1.* from nodes as n1 inner join (select id,max(timestamp) as last from nodes group by id) as n2 on n1.id = n2.id and n1.timestamp = n2.last group by 1,2,3 order by n1.id"):
    nodes_data.append(row)

#Read most recent state from a certain node in Db
def load_node_state(id):
  nodes_data = []
  cur.execute('select n1.* from nodes as n1 where n1.id = {} order by n1.timestamp desc limit 1'.format(id))
  return cur.fetchall()

#node1 = node()

def get_counter(id):
  return cur.execute('select max(counter) from node_counter where id = {} ;'.format(id)).fetchall()[0][0]


def set_counter(id,counter):
    cur.execute("""UPDATE node_counter
                SET counter = ?
                WHERE id = ?""",(counter,id))  
 

def increment_counter(id):
  counter = get_counter(id)
  counter = counter + 1
  set_counter(id,counter)
  conn.commit()
  return counter

  
 
#Get current state from a node
def sync_node(id = 1, cursor = cur, conex = conn, crypto = True, debug = True, mode='check', useCounter = True):
  #Create Alter Connection
  #conn2 = sqlite3.connect('local.db')
  #conn2.row_factory = dict_factory
  #cur2 = conn2.cursor()
  #load node information from database
  node = read_node(id, cursor)
  if debug:
    for i in node:
      print i
  #Load list of parameters
  type = node[1]
  url = node[2]
  route = json.loads(node[3])

  aes = node[5]
  salt = node[6]
  print 'aes = '+node[5]
  print 'salt = '+node[6]

   
  #headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

  #HTTP Request to node
  #CHECAR ESSA PARTE!!!!!!!!!!!!!!!!!!!!!!!!
  #answer = requests.get(url+routeCheck)  
  #answerJ = answer.json()
  
  command = {}  
  
  #load node state from DB
  node_db = load_node_state(id)
  timestamp_node_db = node_db[0][1]
  state_node_db = node_db[0][2]
  counter_db = get_counter(id)
  
  
  payload = {}
  payload['command']={}      
  payload['command']['name'] = mode #check or set
  if mode == 'set':
    payload['command']['value'] = state_node_db
  payload['counter'] = counter_db
  payload['iv'] = urandom(16).encode('hex')
  payload = json.dumps(payload)
  
  #If timestamp from gateway is newer and the state is different, send new state to node
 # if timestamp_node_db > answerJ['time'] and state_node_db <> answerJ['state']:
 #   if type == 'bool':
  print '\n\n'
  print payload

  
  fullUrl = url+'cryp?payload='+encrypt(payload,key=aes,salt=salt) if crypto else url+'semcrypto?payload='+str(payload).encode('hex')
  print '\nURL\n'
  print fullUrl if debug else ''
  answer = None
  newRequest = {}
  # try:
  answer = requests.get(fullUrl)
  answer = answer.text
  print '\n\nanswer: '+answer
  answer = decrypt(answer,key=aes,salt=salt)
  #verifica se houve challenge
  print '\n\nanswer: '+answer
  answer = json.loads(answer)
  print 'RESPOSTA DO ESP8266: ',
  print answer
  if 'challange' in answer['saida'][0]:
    print '\nCHALLENGE REQUISITADA'
    challenge = answer['saida'][0]['challange']
    newRequest['command'] = {}
    newRequest['command']['name'] = 'challange'
    newRequest['command']['response'] = challenge
    newRequest['counter'] = counter_db
    newRequest['iv'] = urandom(16).encode('hex')
    newRequest = json.dumps(newRequest)
    print 'challenge = ',
    print challenge
    print 'mensagem de challenge: ',
    print newRequest
    fullUrl = url+'cryp?payload='+encrypt(newRequest,key=aes,salt=salt)
    newAnswer = requests.get(fullUrl)
    newAnswer = newAnswer.text
    print 'newAnswer = '+newAnswer
    newAnswer = decrypt(newAnswer,key=aes,salt=salt)
    print 'newAnswer decriptado eh  '+newAnswer
    newAnswer = json.loads(newAnswer)
    if mode == 'set':
      increment_counter(id) #####PERIGOSO
      sync_node(id=id,mode=mode)
  
  #Se nao for challenge...
  else:
    newCounter = answer['entrada']['counter']
    if counter_db == newCounter:
      print 'CONTADOR OK'
      print "node sincronizado" if debug else ''
      novoEstado = answer['saida'][0]['state']
      print 'NOVO ESTADO: ',
      print novoEstado
      timestamp = server.getServerTime()
      print 'timestamp = '+str(timestamp)
      # i = [] #List containing all nodes
      # i['id'] = id
      # i['timestamp'] = str(datetime.datetime.now())
      # i['state'] = novoEstado     
  #    timestamp = str(datetime.datetime.utcnow())
  #    timestamp = timestamp[0:10]+'T'+timestamp[11:23]+'Z' 
  ###UPA ESTADO DO ESP PRO BD###
      #Checa se estado eh diferente da DB
      if novoEstado <> state_node_db:
        #query = 'INSERT INTO nodes VALUES ('+str(id)+','+str(timestamp)+?,?)',(int(id),timestamp,novoEstado
        #cur.execute('INSERT INTO nodes VALUES (?,?,?)',(int(id),timestamp,novoEstado))
        cur.execute('INSERT INTO nodes VALUES (2,'+timestamp+',"'+str(novoEstado)+'")')
        conn.commit()
        print "NOVO ESTADO INSERIDO NO BD"
      else:
        print "ESTADO J? ESTA ATUALIZADO"
    else:
      print "CONTADOR ERRADO"
      
      
  # else:
    # print 'CONTADOR recebido '+newCounter+' diferente de '+counter
  # except:
    # print('erro no request ao noh')
    # answer = None
  #If timestamp from node is newer and the state is different, store new state into database
  #if timestamp_node_db < answerJ['time'] and state_node_db <> answerJ['state']:
    #cur.execute('insert into nodes values ({},{},{})'.format(id,answerJ['time'],answerJ['state']))
  
  
  
  #cur2.close()
  #conn2.close()

  increment_counter(id)
  
def sync_all_nodes():
  idList = zip(*load_nodes_info())[0]
  for i in idList:
    try:
      sync_node(i)
    except:
      print "no nao lido"
#Synchronize with Server
#server.main()
  
#

def main(argv):
  pass

if __name__ == '__main__':
  main(sys.argv)


#cur.execute('CREATE TABLE IF NOT EXISTS nodes_info (id INTEGER PRIMARY KEY ASC, type TEXT, url TEXT)')
#cur.commit()
#cur.execute('INSERT INTO nodes_info (1, bool, ))