# -*- coding: utf-8 -*-
import requests
import json
import sqlite3
import time
import esp8266

#DEFINE URL
url = 'https://tcc-projectval.herokuapp.com/'
urlRegister = url + 'users/register'
urlUpdateNodes = url + 'updateNodes'
urlTimestamp = url + 'timestamp'

#Function to convert SQLite3 query to Dict
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

    
    
def register(username = None, password = None):
  
  if username == None:
    print ('Username: ')
    username = raw_input()
  
  if password == None:
    print('Password: ')
    password = raw_input()


  r = {"username" : username, "password" : password}
  r = json.dumps(r)
  #r = json.loads(r)


  print 'post json \n' 
  headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
  answer = requests.post(urlRegister, data=r, headers=headers)
  answer = answer.json()
  login = {"user" : username, "password" : password, "raspberryId" : answer['id'], "token" : answer['token']}

  with open('login.json', 'w') as outfile:
       json.dump(login, outfile, sort_keys = False, indent = 4,
                 ensure_ascii = False)    
    
def getServerTime():
  return requests.get(urlTimestamp).text


def syncServer(debug = False):
  
  #Create Local Database Connection
  conn = sqlite3.connect('local.db')
  conn.row_factory = dict_factory
  cur = conn.cursor() 
  
  
  #Read the login information
  param = json.load(open('login.json','r'))


  #Read Timestamps
  nodes = []
  for row in cur.execute("select n1.* from nodes as n1 inner join (select id,max(timestamp) as last from nodes group by id) as n2 on n1.id = n2.id and n1.timestamp = n2.last group by 1,2,3"):
    nodes.append(row)

  #Add Nodes to list of parameters
  param['nodes'] = nodes
  print (param if debug else '')
  with open('param_server.txt', 'w') as outfile:
    json.dump(param, outfile)
  
  #POST into server
  print (('POST into ' +  urlUpdateNodes + '\n') if debug else '')
  headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
  answer = requests.post(urlUpdateNodes, json=param, headers=headers)
  print (answer.text if debug else '')
  answer = answer.json()

  with open('answer_server.txt', 'w') as outfile:
    json.dump(answer, outfile)
  
  #Creating list containing all nodes received by the Server
  lista = [] #List containing all nodes
  for i in answer:
    id = i['id']
    timestamp = i['timestamp']
    state = i['state']
    state = str(state).lower()# if type(state) is str else state
    node = (id, timestamp, str(state))
    lista.append(node)

  # try:
    # #INSERT nodes into database
    
    # #cur.executemany('INSERT INTO nodes VALUES (?,?,?)',lista)
    # conn.commit()
  # except:
    # print "erro ao inserir"
  
  print "lista " + str(len(lista))
  for i in lista:
    print i
  print "\nnodes " + str(len(nodes))
  for i in nodes:
    print i
  
  
  for i in lista:
    try:
      import esp8266
      for j in nodes:
        if int(j['id'])==int(i[0]) and i[1] > j['timestamp'] and i[2] <> j['state']:
          print "\n ESTADOS LOCAL E DO SERVER DIFERENTES, INSERINDO NO BD LOCAL....\n"
          cur.execute('INSERT OR IGNORE INTO nodes VALUES (?,?,?)',i)
          conn.commit()
          try:
            esp8266.sync_node(id = int(i[0]), mode = 'set')
            print "no "+str(i[0])+" inserido com sucesso"
          except:
            "server.py: erro no sync_node"
    except:
      print "erro ao inserir. possivel duplicado"
    
    
  if debug == True:
    for i in cur.execute("select n1.* from nodes as n1 inner join (select id,max(timestamp) as last from nodes group by id) as n2 on n1.id = n2.id and n1.timestamp = n2.last group by 1,2,3"):
     print i

  cur.close()
  conn.close()
  
  return lista
  
def main():
  while True:
    time.sleep(5)
    syncServer()
   
if __name__ == '__main__':
    main()
    pass
    
    
# cur.execute("delete from nodes")
# conn.commit()
# cur.executemany('INSERT INTO nodes VALUES (?,?,?)',[('1','2017-08-17T17:44:14.184Z','true'),('2','2017-08-20T17:44:14.184Z','false')])
# conn.commit()
# for i in cur.execute("select * from nodes"):
 # print i