#include <ESP8266WiFi.h>
//#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Crypto.h>
#include <ArduinoJson.h>



#define BLOCK_SIZE 16
extern uint8_t key[32];
extern uint8_t iv[16];
extern byte;
extern const char* ssid;
extern const char* password;
int counterGlob = 0;
int challange = -1;

typedef void (*PsdAPIRt)(JsonObject& inputJson);
ESP8266WebServer server(80);

const int led = 13;
const int led2 = 2;
String ledTime = "";
String state = "";

void bufferSize(byte* text, int &len)
{
  int buf = round(len / BLOCK_SIZE) * BLOCK_SIZE;
  len = (buf < len) ? buf + BLOCK_SIZE : buf;
}
int char2int(char input)
{
  if(input >= '0' && input <= '9')
    return input - '0';
  if(input >= 'A' && input <= 'F')
    return input - 'A' + 10;
  if(input >= 'a' && input <= 'f')
    return input - 'a' + 10;
  else
  {
    Serial.println("error");
    return input - 0;
  }
}

void hex2bin(const char* src, byte* target, int len)
{
  for (int i=0; i < len; i = i + 2)
  {
    *(target++) = char2int(*src)*16 + char2int(src[1]);
    src += 2;
  }
}

char* decodeAndDecript (String* message, int length) {

  //Declaration
  char holder[length+1];
  holder[length] = '\0';
  int halfLength = length/2;
  int halfLengthMsg = halfLength - 48;
  byte decoded[halfLength+1];
  decoded[halfLength] = '\0';
  char out [halfLengthMsg+1];
  out[halfLengthMsg] = '\0';

  //Get Msg
  strcpy(holder, message->c_str());

  //Decode
  hex2bin(holder, decoded, length);

  //Separate IV MSG and hash
  memcpy((char*)iv, decoded, 16*(sizeof(uint8_t)));

  //checkHash
  if (!checkHash(decoded, halfLengthMsg+16)){
    Serial.println("Unecognized hash");
    
    return "{\"hmacerror\":true}\0";
  }
  else
  {
    Serial.println("Recognized hash");
  }

  //Decrypt
  decrypt(decoded+16, out, halfLengthMsg);
  return out;
}

void encrypt(char* plain_text, char* output, int length)
{
  byte enciphered[length];
  AES aesEncryptor(key, iv, AES::AES_MODE_256, AES::CIPHER_ENCRYPT);
  aesEncryptor.process((uint8_t*)plain_text, enciphered, length);
  int encrypted_size = sizeof(enciphered);
  char encoded[encrypted_size];
  //base64_encode(encoded, (char*)enciphered, encrypted_size);
  strncpy(output, encoded, length);

}
bool checkHash(byte* msg, int len){
  byte auth[32];
  //for(int i = 0; i < len+32; i++)
  // {
  //  Serial.print(msg[i]);
  //  Serial.print(",");
  //}
  //Serial.println(".");
  SHA256HMAC hmac(c_salt, 32);
  hmac.doUpdate(msg, len);
  hmac.doFinal(auth);
  int n = memcmp(msg+len, auth, 32);
  if (n == 0)
    return true;
  else
    return false;
}

bool checkCounter(JsonObject& totalJson){
  if ("challange" == totalJson["entrada"]["command"]["name"]){
    if (challange == totalJson["entrada"]["command"]["response"]){
      counterGlob = (int)totalJson["entrada"]["counter"] +1;
      return true;
    }
    else
    return false;
  }
  else if (counterGlob == -1 || counterGlob != totalJson["entrada"]["counter"]){
    return false;
  }
  else{
      counterGlob++;
      return true;
  }

}
void decrypt(byte* enciphered, char* output, int length)
{ 
  //Declaration
  byte deciphered[length+1];
  deciphered[length] = '\0';
  
  bufferSize(enciphered, length);
  AES aesDecryptor(key, iv, AES::AES_MODE_256, AES::CIPHER_DECRYPT);
  aesDecryptor.process((uint8_t*)enciphered, deciphered, length);
  strcpy(output, (char*)deciphered);
  output[length] = '\0';
}

void handleRoot() {
  
  state = server.arg("state");
  if (state == "on") digitalWrite(led2, HIGH);
  else if (state == "off") digitalWrite(led2, LOW);
  ledTime = getTime();
  String buf = "";
  buf += "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
  buf += "<h3> ESP8266 Web Server</h3>";
  buf += "<h3> Last Change at "+ ledTime +"</h3>";
  buf += "<p>GPIO4 <a href=\"?state=on\"><button>ON</button></a> <a href=\"?state=off\"><button>OFF</button></a></p>";
  buf += "<h4>Copiado por Diego Villalobos</h4>";
  buf += "</html>\n";

  server.send(200, "text/html", buf);
}

void ledState(JsonObject& totalJson) {
  JsonObject& saida = ((JsonArray&)totalJson["saida"]).createNestedObject();
  saida["name"] = "set";
  digitalWrite(led, 1);
  String state = totalJson["entrada"]["command"]["value"];
  saida["state"] = state;
  //if (state == "true") digitalWrite(led2, HIGH);
  //else if (state == "false") digitalWrite(led2, LOW);
  digitalWrite(led, 0);
  checkLedState(totalJson);
}


void criptoTest() {
  
  state = server.arg("payload");
  String inputStr = "";
  DynamicJsonBuffer jsonBuffer;
  DynamicJsonBuffer jsonBufferIn;
  JsonObject& totalJson = jsonBuffer.createObject();
  JsonArray& saida = totalJson.createNestedArray("saida");
  const char* decryptedChar = decodeAndDecript(&state ,state.length());
  totalJson["entrada"] = jsonBufferIn.parseObject(decryptedChar);
  if(totalJson["entrada"]["hmacerror"]){
    totalJson.printTo(inputStr);
    Serial.println("DealingWithJunk");
    server.send(200, "application/json", inputStr);
    return;
  }
  //check counter
  if (!checkCounter(totalJson)){
    JsonObject& saidaO = saida.createNestedObject();
    saidaO["name"] = "error";
    saidaO["type"] = "counter";
    challange = random(0,10000000);
    saidaO["challange"] = challange;
    totalJson.printTo(inputStr);
    server.send(200, "application/json", inputStr);
    return;
  }
  
  String apiToCall =  totalJson["entrada"]["command"]["name"]; 
  // else if decider tree such bulky useles code but meh
  if (apiToCall == "set"){
    ledState(totalJson);
  }
  else if (apiToCall == "check"){
    checkLedState(totalJson);
  }
  totalJson.printTo(inputStr);
  server.send(200, "application/json", inputStr);
  return;
}

void checkLedState(JsonObject& totalJson) {
  JsonObject& saida = ((JsonArray&)totalJson["saida"]).createNestedObject();
  saida["name"] = "check";
  digitalWrite(led, 1);
  state = digitalRead(led2);
  if (state == "1")  saida["state"] = "true";
  else if (state == "0") saida["state"] = "false";
  else saida["state"] = "error";
  digitalWrite(led, 0);
}

String getTime() {
  WiFiClient client;
  while (!!!client.connect("google.com", 80)) {
    Serial.println("connection failed, retrying...");
  }

  client.print("HEAD / HTTP/1.1\r\n\r\n");
 
  while(!!!client.available()) {
     yield();
  }

  while(client.available()){
    if (client.read() == '\n') {    
      if (client.read() == 'D') {    
        if (client.read() == 'a') {    
          if (client.read() == 't') {    
            if (client.read() == 'e') {    
              if (client.read() == ':') {    
                client.read();
                String theDate = client.readStringUntil('\r');
                client.stop();
                return theDate;
              }
            }
          }
        }
      }
    }
  }
} 

void handleNotFound(){
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}

void setup(void){
  pinMode(led, OUTPUT);
  pinMode(led2, OUTPUT);
  digitalWrite(led2, 0);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
  Serial.println();
  
  server.on("/", handleRoot);
  server.on("/cryp", criptoTest);


  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}
