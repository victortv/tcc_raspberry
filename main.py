from Tkinter import *
import json

import esp8266
import server

import sqlite3
from threading import Thread
import time

from multiprocessing import Process

def syncServer():
  while True:
    time.sleep(2)
    a = server.syncServer(debug = False)

def syncNode():
  while True:
    time.sleep(5)
    try:
      esp8266.sync_all_nodes()
    except:
      print "erro processo noh"
    
def scriptFirst():
  import register_node as rn
  rn.main() #generate new constant file
  os.system('./esp8266/compila_upa.sh') #compile and upload to esp8266

  

  
    
class ControlPanel:

  def login(self,root,registrar,user,senha):
    print user + '    ' + senha
    
    if registrar:
      server.register(user,senha)
    else:
      #verify if user and password are correct
      pass
    root.destroy()
  
  def loginScreen(self,master):
    master.title('Smart Home Control Panel')

    labelLogin=StringVar()
    labelLogin.set("Entre com Usuario e Senha")
    LabelLogin = Label(master,textvariable=labelLogin,height=4)
    LabelLogin.pack()
    
    labelUser=StringVar()
    labelUser.set("Usuario")
    LabelUser = Label(master,textvariable=labelUser,height=1)
    LabelUser.pack()    
    
    varUser=StringVar(None)
    entryUser=Entry(master,textvariable=varUser,width=20)
    entryUser.pack()
    
    labelSenha=StringVar()
    labelSenha.set("Senha")
    LabelSenha = Label(master,textvariable=labelSenha,height=1)
    LabelSenha.pack()
    
    varSenha=StringVar(None)
    entrySenha=Entry(master,textvariable=varSenha,width=20)
    entrySenha.pack()

    #Generate Buttons
    buttonEntrar = Button(master, text='Entrar',width=10, command=lambda:self.login(master,False,entryUser.get(),entrySenha.get()))
    buttonEntrar.pack(side='left')

    buttonRegistrar = Button(master, text='Registrar',width=10, command=lambda:self.login(master,True,entryUser.get(),entrySenha.get()))
    buttonRegistrar.pack(side='right')
  
    return entryUser.get()
  
  def mainScreen(self,master,username):
    master.title('Smart Home Control Panel')
    
    labelLogin=StringVar()
    labelLogin.set("Bem-vindo " + username)
    LabelLogin = Label(master,textvariable=labelLogin,height=4)
    LabelLogin.grid(row=0,column=1)
    
    nodes = esp8266.load_nodes_info()
    
    for indexRow,i in enumerate(nodes):
      for indexCol,j in enumerate([0,4,2]): #ID, Name, IP
        labelNode=StringVar()
        labelNode.set(i[j])
        LabelNode = Label(master,textvariable=labelNode,height=0)
        LabelNode.grid(row=indexRow+1,column=indexCol)
    
    buttonNewNode = Button(master, text='New Node',height=2,width=10, command=lambda:Process(target = scriptFirst, args = ()).start())
    buttonNewNode.grid(row=indexRow+2,column=0,pady=5)
    
    buttonSS = Button(master, text='Sync Server',width=10, command=lambda:Process(target = syncServer, args = ()).start())
    buttonSS.grid(row=indexRow+3,column=2)
    
    buttonAPI = Button(master, text='Run API',width=10, command=lambda:Process(target = esp8266.run_api, args = ()).start())
    buttonAPI.grid(row=indexRow+3,column=1)
    
    buttonSN = Button(master, text='Sync Nodes',width=10, command=lambda:Process(target = syncNode, args = ()).start())
  
    buttonSN.grid(row=indexRow+3,column=0)
    
    
  def __init__(self):
   

    
    root = Tk()
    self.loginScreen(root)
    root.mainloop()    
    
   
   
    j = json.load(open('login.json'))
    self.username = j['user']
    self.raspberryId = j['raspberryId']
    root = Tk()
    self.mainScreen(root, self.username)
    root.mainloop()


    
    
def main():

  
  obj = ControlPanel()
  
if __name__ == '__main__':
  main()
