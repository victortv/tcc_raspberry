# -*- coding: utf-8 -*-
import requests
import json
import sqlite3
import server
from os import urandom

def main(id = -1, type = 'bool', url = 'http://localhost:3000/', aes = None, salt = None):
  
  with open('constants.h', 'w') as output:
    output.write("const char* c_id = " + str(id) + ";\n")
    output.write('const char* c_ssid = "SulzHall";\n')
    output.write('const char* c_password = "995547192";\n')
    output.write('byte c_aesKey[32] = {')
    if aes == None:
        output.write('0x31,0x5f,0x71,0x24,0xef,0x11,0xca,0xa1,0x84,0x9b,0x54,0xca,0xf9,0x30,0x79,0xfd,0x2a,0xc4,0x2d,0x59,0x1b,0xb8,0xa6,0x88,0xaa,0x8a,0x8e,0x6b,0xb6,0x3d,0x3b,0xd8')
    else:
      for j in range(0,len(aes)):
        output.write('0x'+aes[j]+aes[j+1])
        j+=2
    output.write('};\n')
    
    output.write('byte c_salt[32] = {')
    if salt == None:
        output.write('0xfd,0x07,0xfb,0xdd,0x03,0x81,0x74,0xd0,0x68,0x1f,0xdb,0x98,0x35,0x7b,0x4b,0x46,0x0f,0x7b,0x55,0xc8,0x89,0x8a,0x22,0xc0,0xee,0x51,0x92,0x12,0xeb,0x07,0xcc,0xcb')
    else:
      salt = urandom(16).encode('hex')
      for j in range(0,len(aes)):
        output.write('0x'+salt[j]+salt[j+1])
        j+=2
    output.write('};\n')      
  
if __name__ == '__main__':
  main()